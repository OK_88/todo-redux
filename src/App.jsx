import { useState } from "react";
import TodoList from "./components/TodoList/TodoList";
import InputField from "./components/InputField/InputField";
import { useDispatch } from "react-redux";
import { addTodo } from "./store/todoSlice";
import "./App.css";

function App() {
  const [text, setText] = useState("");
  const dispatch = useDispatch();

  const addTask = () => {
    if (text.trim().length) {
      dispatch(addTodo({ text }));
      setText("");
    }
  };

  return (
    <div className="App">
      <InputField handleSubmit={addTask} handleInput={setText} text={text} />

      <TodoList />
    </div>
  );
}

export default App;
